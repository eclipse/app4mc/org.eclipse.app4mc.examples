
# Eclipse APP4MC - Examples

This is the repository of additional examples related to Eclipse APP4MC:

    a4mcar                  | A4MCAR Project
    rover                   | Software (C/C++) for APP4MC Rover project
    RTFParallella           | Framework for embedded applications on Adapteva Parallella platform
    WATERS-FMTV-challenges  | WATERS Industrial Challenges 2016, 2017, 2019

For further information see the readme files in the subdirectories.

## License

[Eclipse Public License (EPL) v2.0][1]

[1]: https://www.eclipse.org/legal/epl-2.0/