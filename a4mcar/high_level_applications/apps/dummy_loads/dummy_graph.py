#!/usr/bin/python

# Copyright (c) 2017 Eclipse Foundation and FH Dortmund.
# All rights reserved. This program and the accompanying materials
# are made available under the terms of the Eclipse Public License v1.0
# which accompanies this distribution, and is available at
# http://www.eclipse.org/legal/epl-v10.html
#
# Description:
#    A4MCAR Project - Application that creates a dummy graph using threads
#
# Author:
#    M. Ozcelikors <mozcelikors@gmail.com>

import sys
import os
import string
import math
import datetime
import time
import threading
from threading import Lock
from time import localtime, strftime
import prctl
import numpy
import imp
import string
import signal

aprocessSrc = imp.load_source('aprocess','../touchscreen_display/aprocess.py')

# Set-up
matrix_size = 190
periods = 0.5
thread_names =			["A",   "B",   "C", "D",  "E",  "F",  "G",  "H",  "I",  "J"]
instruction_size_scales =	[ 1,     3,    4,    9,    1,    2,    5,    3,    2,    4 ]
#mapped_cores =			["3","3","3","3","3","3","3","3","3","3"]
mapped_cores =			["0-3","0-3","0-3","0-3","0-3","0-3","0-3","0-3","0-3","0-3"]
threads = 			[None] * 10

# 1-byte Labels
BF = "0"
BG = "0"
BJ = "0"
CH = "0"
AE = "0"
EH = "0"
DJ = "0"
HI = "0"
GI = "0"
FI = "0"

def StopAllThreads():
	global thread_names
	global threads
	
	for i in range (0, len(thread_names)):
		try:
			threads[i].join(0)
		except Exception as inst:
			print inst

# Join all threads gracefully when SIGINT received, (Ctrl+C)
def Signal_Handler (signal, frame):
	StopAllThreads()
	sys.exit(0)


# Worker function
def Worker(name, instruction_size_scale, core_affinity):
	global aprocessSrc

	# Set-up parameters
	global matrix_size
	global periods
	
	# Labels
	global BF
	global BG
	global BJ
	global CH
	global AE
	global EH
	global DJ
	global HI
	global GI
	global FI
	read_label = "n"

	#Timing Related ---start
	_thr_DEADLINE = periods
	_thr_START_TIME = 0
	_thr_END_TIME = 0
	_thr_EXECUTION_TIME = 0                  
	_thr_PREV_SLACK_TIME = 0
	_thr_PERIOD = periods
	#Timing Related ---end

	#Initialize thread and append it to the global process list
	this_thread = aprocessSrc.aprocess(name, 1, "../../logs/timing/"+name+".inc", 0, name, "None", 1)
	this_thread.UpdateThreadIDAndRunning()
	this_thread.SetCoreAffinityOfThread(core_affinity)
	prctl.set_name(name) #Sets the thread title for linux kernel
	
	while True:
		#Timing Related
		_thr_START_TIME = time.time()
		_thr_PREV_SLACK_TIME = _thr_START_TIME - _thr_END_TIME
                
		#TASK CONTENT starts here
		# Dummy read label
		if (name == "F"):
			read_label = BF
		elif (name == "G"):
			read_label = BG
		elif (name == "E"):
			read_label = AE
		elif (name == "H"):
			read_label = EH
			read_label = read_label + CH
		elif (name == "I"):
			read_label = FI
			read_label = read_label + GI
			read_label = read_label + HI
		elif (name == "J"):
			read_label = BJ
			read_label = read_label + DJ
		
		# Dummy work
		for i in range(0,instruction_size_scale):
			a=numpy.random.random([matrix_size, matrix_size])
			b=numpy.random.random([matrix_size, matrix_size])
			c=numpy.mean(a*b)
                
		# Dummy write label
		if (name == "B"):
			BF = "w"
			BG = "w"
			BJ = "w"
		elif (name == "F"):
			FI = "w"
		elif (name == "C"):
			CH = "w"
		elif (name == "G"):
			GI = "w"
		elif (name == "A"):
			AE = "w"
		elif (name == "E"):
			EH = "w"
		elif (name == "H"):
			HI = "w"
		elif (name == "D"):
			DJ = "w"
		
		#Create timing log
		try:
			file_obj = open(str(this_thread.aplogfilepath), "w+r")
		except Exception as inst:
			print inst
			#dbg = 1
		_thr_END_TIME = time.time()
		_thr_EXECUTION_TIME = _thr_END_TIME - _thr_START_TIME
		try:
			file_obj.write(str(_thr_PREV_SLACK_TIME)+' '+str(_thr_EXECUTION_TIME)+' '+str(_thr_PERIOD)+' '+str(_thr_DEADLINE))
			file_obj.close()
		except Exception as inst:
			#print inst
			dbg = 1
                
		# Delay		
		if (_thr_PERIOD > _thr_EXECUTION_TIME):
			time.sleep(_thr_PERIOD - _thr_EXECUTION_TIME)
			
#Main
def main():
	global thread_names
	global threading
	global instruction_size_scales
	global mapped_cores

	#Threads start
	for i in range (0, len(thread_names)):
		try:
			threads[i] = threading.Thread(target=Worker, args=(thread_names[i], instruction_size_scales[i], mapped_cores[i]), name=thread_names[i])
			threads[i].setDaemon(True)
			threads[i].start()
		except Exception as inst:
			print inst

	done = False
	signal.signal(signal.SIGINT, Signal_Handler)
	signal.pause()

	while not done:
		# Main thread doing nothing.
		try:
			time.sleep(1)
		except (KeyboardInterrupt, SystemExit):
			StopAllThreads()
			sys.exit(0)
			
if (__name__=="__main__"):
	main()
