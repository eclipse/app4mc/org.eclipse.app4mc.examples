#!/bin/bash

#	Copyright (c) 2017 FH Dortmund.
#	All rights reserved. This program and the accompanying materials are made available under the
#	terms of the Eclipse Public License v1.0 which accompanies this distribution, and is available at
#	http://www.eclipse.org/legal/epl-v10.html

# Kills a process given its name
# Usage: bash ProcessKill.sh <process_name> 
# ATTENTION! Do not run as root

args=("$@")
process_name=${args[0]}

pid=$(pgrep -u root -f $process_name -n ) #Newest result

#Place the task on a specific core.
sudo kill -9 $pid &&
echo "Process $process_name with PID=$pid on core has been killed."

