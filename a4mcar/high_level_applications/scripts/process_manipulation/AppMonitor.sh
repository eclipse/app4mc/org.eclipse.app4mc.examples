#!/bin/bash

#	Copyright (c) 2017 FH Dortmund.
#	All rights reserved. This program and the accompanying materials are made available under the
#	terms of the Eclipse Public License v1.0 which accompanies this distribution, and is available at
#	http://www.eclipse.org/legal/epl-v10.html

# Prints the process stats given the process name
# Usage: bash AppMonitor.sh <process_name> 
# ATTENTION! Do not run as root

args=("$@")
process_name=${args[0]}
pid=$(pgrep -f $process_name -n ) #Newest result

sudo perf stat -p $pid

