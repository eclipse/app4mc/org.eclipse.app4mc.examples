______________________________________________________________________________________________

WATERS 2017

International Workshop on Analysis Tools and Methodologies for Embedded and Real-time Systems


https://waters2017.inria.fr/



WATERS community forum

https://waters2017.inria.fr/community-forum/


______________________________________________________________________________________________

FMTV 2017

Formal Methods for Timing Verification (FMTV) challenge


https://waters2017.inria.fr/challenge/#Challenge17



The 2017 FMTV Challenge (BOSCH)


http://ecrts.eit.uni-kl.de/forum/viewtopic.php?f=32&t=85

______________________________________________________________________________________________
