
WATERS - International Workshop on Analysis Tools and Methodologies for Embedded and Real-time Systems

https://www.ecrts.org/archives/index9140.html?id=waters

__________________________________________________________

WATERS Industrial Challenges 2016, 2017, 2019
__________________________________________________________


The documents, references and the original model files of the
industrial challenges are stored in the following subfolders:

 - fmtv-challenge-2016/
 - fmtv-challenge-2017/
 - fmtv-challenge-2019/
__________________________________________________________

If you use the Eclipse APP4MC products the recommended way to load
the challenge models is an import of the zipped projects.


1. Get the APP4MC platform

    https://projects.eclipse.org/projects/technology.app4mc/downloads

2. Import the projects into workspace

    Import... > General > Existing Projects into Workspace

    Select archive file

The following archive files contain migrated versions
of the original challenge models with manual fixes:

 - WATERS_FMTV_Challenges__Eclipse_projects__APP4MC_v0.9.6.zip

