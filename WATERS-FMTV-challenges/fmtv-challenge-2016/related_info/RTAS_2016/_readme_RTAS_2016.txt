______________________________________________________________________________________________

RTAS 2016

IEEE REAL-Time and Embedded Technology and Applications Symposium


http://2016.rtas.org

______________________________________________________________________________________________


Demo Session

http://2016.rtas.org/demos/



Demonstration of the FMTV 2016 Timing Verification Challenge

Arne Hamann, Dirk Ziegenbein, Simon Kramer and Martin Lukasiewycz



http://2016.rtas.org/wp-content/uploads/2016/04/RTAS-demo-paper-3.pdf

http://2016.rtas.org/wp-content/uploads/2016/04/RTAS-demo-presentation-3.pdf