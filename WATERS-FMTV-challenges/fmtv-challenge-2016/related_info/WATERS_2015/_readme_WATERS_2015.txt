______________________________________________________________________________________________

WATERS 2015

International Workshop on Analysis Tools and Methodologies for Embedded and Real-time Systems


https://waters2015.inria.fr/

______________________________________________________________________________________________


Real world automotive benchmark for free (BOSCH)


http://ecrts.eit.uni-kl.de/forum/viewtopic.php?f=20&t=23


Title: Real World Automotive Benchmark For Free

Authors:
Simon Kramer, Dirk Ziegenbein, Arne Hamann
(Corporate Research, Robert Bosch GmbH, Renningen, Germany)

Abstract:
The progress and comparability of real-time analysis methods that are applicable to real-world
is slowed by the absence of realistic benchmarks, mainly due to intellectual property (IP) concerns.
We propose a method that supports the generation of realistic but IP free benchmark sets. Further,
we provide the application characteristics of a specific real-world automotive software system.

Keywords:
benchmarks, timing analysis, automotive software


Attached paper:

WATERS15_Real_World_Automotive_Benchmark_For_Free.pdf	(176.2 KiB)


Slides:

Real World Automotive Benchmarks For Free - ECRTS - WATERS 2015.pdf	(520.25 KiB)

